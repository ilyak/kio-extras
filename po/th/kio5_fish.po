# translation of kio_fish.po to Thai
#
# Thanomsub Noppaburana <donga_n@yahoo.com>, 2005.
# Thanomsub Noppaburana <donga.nb@gmail.com>, 2008, 2010.
msgid ""
msgstr ""
"Project-Id-Version: kio_fish\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:32+0000\n"
"PO-Revision-Date: 2010-02-14 01:18+0700\n"
"Last-Translator: Thanomsub Noppaburana <donga.nb@gmail.com>\n"
"Language-Team: Thai <thai-l10n@googlegroups.com>\n"
"Language: th\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.0\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: fish.cpp:322
#, kde-format
msgid "Connecting..."
msgstr "กำลังเชื่อมต่อ..."

#: fish.cpp:656
#, kde-format
msgid "Initiating protocol..."
msgstr "กำลังเริ่มการทำงานโพรโทคอล..."

#: fish.cpp:693
#, kde-format
msgid "Local Login"
msgstr "ล็อกอินภายในระบบ"

#: fish.cpp:695
#, fuzzy, kde-format
#| msgid "SSH Authorization"
msgid "SSH Authentication"
msgstr "ตรวจสอบสิทธิ์ผ่าน SSH"

#: fish.cpp:732 fish.cpp:747
#, kde-format
msgctxt "@action:button"
msgid "Yes"
msgstr ""

#: fish.cpp:732 fish.cpp:747
#, kde-format
msgctxt "@action:button"
msgid "No"
msgstr ""

#: fish.cpp:830
#, kde-format
msgid "Disconnected."
msgstr "ยุติการเชื่อมต่อแล้ว"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "ถนอมทรัพย์ นพบูรณ์"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "donga.nb@gmail.com"
